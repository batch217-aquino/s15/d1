console.log("Mabuhay!");

// JS - is aloosely type Programming Language

// alert("Hello Again!")

console.log( "Hello World!" );

console.
log
("Hello World!");

// [SECTION] - Variables
// used to contain data
// usually stored in a computer's memory.


let myVaiable; 

console.log(myVaiable);

let mySecondVariable = 2;
console.log(mySecondVariable);

let productName = "desktop computer";
console.log(productName);

let friend;
friend = "Kate";
friend = "Jane";
friend = "Lawrence";
console.log(friend);

// Syntax const variableValue

const pet = "Bruno"
// pet = "Lala"
console.log(pet);



// [SECTION] - Variables


// Local and Global variables

let outerVar = "hello"; //globalVar

{
    let innerVar = "Hello World!" //localVar
    console.log(innerVar);
}
console.log(outerVar);

const outerEx = "GlobalVar";
{
    const innerEx = "localVar";
    console.log(innerEx)
}
console.log(outerEx)

// Mutiple Declation

let productCode = "DC017", productBrand = "Dell";

// let productCode = "DC017";
// let productBrand = "Dell";

console.log(productCode, productBrand);



// [SECTION] - Data types
// Strings - creates words ("")

// concatenating strings
let country = "Philippines";
let province = "Cebu City";
let fullAdd = province + ", " + country;
console.log(fullAdd);


let greeting = "I live in" + ", " + country;
console.log(greeting);


let mailAdd = "Metro Manila\n\nPhilipphines";
console.log(mailAdd);

let msg = "John's employees went home early.";
msg = 'John\'s employees went \"home early"\.'
console.log(msg);


// Numbers
let hcount = 26;
console.log(hcount);

// let grade = 98.7;
// console.log(grade)

let distance = 2e10;
console.log(distance);

// concat string and numbers
// console.log("My grade last sem is " + grade);

// Bollean

let isM = false;
let inG = true;
console.log("isM: " + isM);
console.log("inG: " + inG);

// Array
let english = 90;
let math = 100;
let arts = 98;

let grades = [98.7, 92.1, 90.2, 92.6];
console.log(grades);
let details = ["John", "Smith", 32, true]
console.log(details)

// Objects
let person = {
    fullName: "Juan",
    age: 35,
    isMarried: false,
    contacts: ["098765321", "0912345677"],
    address: {
        houseNumber: "345",
        city: "Manila",
    }

}
console.log(person)



const anime = ["one piece", "one punch", "attack on titan"];
anime[2] = "Kimetsu";
console.log(anime)